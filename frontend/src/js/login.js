import { refreshContent } from "../index.js";
import { persistTokenToStorage } from "./util.js";

export const API_URL = "http://localhost:8000";

function showLoginError(error) {
  //TODO: toon de alert en geef de error weer.
}

function storeTokenAndRefresh(token) {
  //TODO: sla de token op en refresh de index pagina (refreshContent()) daarna.
}

export function login(username, password) {
  //TODO
  // Voer een fetch POST request uit naar API_URL/login om de token op te halen, return de token
}

export function addLoginHandler() {
  //TODO: zorg dat de loginButton element id overeenkomt met die in je login.html pagina
  const loginButton = document.getElementById("btn_login");
  loginButton.addEventListener("click", (event) => {
    event.preventDefault();
    //TODO
    // Haal username en password uit het formulier
    login(username, password)
      .then((result) => {
        console.log("Result:", result);
        //TODO:
        //storeTokenAndRefresh(result.token);
      })
      .catch((error) => showLoginError(error));
  });
}

import config from "config";
import jwt from "jsonwebtoken";
import { MSG_NO_TOKEN } from "./util/errorHander";
// voor config en opties, zie: https://www.npmjs.com/package/jsonwebtoken

export async function createJWT(user) {
  try {
    const signingKey = config.get("Auth.secret");
    // +/- drie maanden geldig, tijd in seconden. Normaal geven we een (veel) korte expiration date, maar omdat we
    // expiration van een token niet opvangen in deze opgave (dat zou ons te ver brengen), houden we de token zeer lang geldig.
    const expirationDate = Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 90;
    const token = jwt.sign(
      {
        issuer: "KDG_test_jwt_backend",
        exp: expirationDate,
        userId: user.id,
        username: user.username,
      },
      signingKey
    );
    return token;
  } catch (error) {
    console.error("createJWT:", error);
    throw new Error("Could not create JWT");
  }
}

export async function verifyJWT(token) {
  try {
    // Returnt het gedecodeerde token als die ok is, gooit een error als het niet geverifieerd kan worden.
    return jwt.verify(token, config.get("Auth.secret"));
  } catch (error) {
    throw new Error("Invalid token.");
  }
}

function stripBearerFromAuthHeader(authHeader) {
  if (authHeader.startsWith("Bearer")) {
    // Haal "Bearer" string (die aangeeft dat het om een Bearer token gaat: https://security.stackexchange.com/questions/108662/why-is-bearer-required-before-the-token-in-authorization-header-in-a-http-re)
    const withoutBearer = authHeader.replace("Bearer", "");
    // Verwijdert leading en trailing spaties.
    return withoutBearer.trim();
  }
}

export async function extractTokenFromRequest(req) {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) throw new Error(MSG_NO_TOKEN);
    return stripBearerFromAuthHeader(authHeader);
  } catch (error) {
    throw error;
  }
}

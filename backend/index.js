import express from "express";
import config from "config";
import cors from "cors";

import addRoutes from "./routing/addRoutes";
import seed from "./database/seedUsers";
import { handleError } from "./util/errorHander";

// Haal de nodige config op.
const serverConfig = config.get("Server");
const port = serverConfig.port;
const host = serverConfig.host;

/**
 * Deze functie voegt users toe aan de gesimuleerde database. Indien we met
 * een echte database zouden werken zouden we deze functie natuurlijk
 * nooit hoeven uit te voeren.
 */
seed();

// Creëer de express (webserver) app
const app = express();
// Voeg Cross origin resource sharing (CORS) config toe (deze laat gewoon stanaard alles toe)
app.use(cors());
// Parse inkomende request bodies als json.
app.use(express.json());
// Voeg de routes die we gaan openstellen voor gebruikers (de frontend) aan de app toe.
addRoutes(app);
app.use((error, req, res, next) => {
  const { statusCode, message } = handleError(error);
  res.statusMessage = message;
  res.status(statusCode).send();
});

// start de webserver op de gespecifieerde poort.
const server = app.listen(port, host, () => {
  console.log("Voorbeeld app gestart op at http://%s:%s", host, port);
});

import bcrypt from "bcrypt";

export const hashPassword = async (plainTextPassword) =>
  await bcrypt.hash(plainTextPassword, 5);

export const compare = async (plainTextPassword, hashedFromDB) =>
  await bcrypt.compare(plainTextPassword, hashedFromDB);

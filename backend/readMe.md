# Authentication met JWT: Backend

## Node

Het 'npm start' commando dat in `package.json` gedefinieerd is, bevat de node-flag `--experimental-specifier-resolution=node`. Deze flag (waarbij we de mode naar 'node' zetten ipv de default optie 'explicit') zorgt er voor dat we `import` kunnen gebruiken zonder de extensie van elke file mee te geven aan de import.
In 'explicit' mode zouden we bv de import:

`import addRoutes from "./routing/addRoutes";`

Moeten doen als (merk de `.js`-extensie op):

`import addRoutes from "./routing/addRoutes.js";`

Zie ook:

- dit [Stackoverflow issue][issue]
- deze Node changelog: [ESModules in Node v14][esmodules-node]
- [deze guide][esmodules-node-guide] over het gebruik van ESModules en de vergelijking met CJS

## JWT

Om de inhoud van een token te bekijken, gebruik: https://jwt.io/

secret: In de `config/default.json` file zit een 'secret'. Dit is een sleutel die gebruikt wordt voor het versleutelen van je JWT. Zoals de inhoud van die key aangeeft, is het eigenlijk niet de bedoeling om die daar te laten staan. Als die config file namelijk in version control terecht komt, kan iedereen met toegang tot je repository je secret zien, en da's een veiligheidsrisico.
Een betere manier is om de key bv in een environment variable te stoppen en in te stellen op de server, en dan in te lezen in de config file, maar dat valt buiten de scope van deze opdracht.

[issue]: https://stackoverflow.com/questions/64242186/node-cant-find-modules-without-js-extension "js extension Stackoverflow issue"
[esmodules-node]: https://nodejs.org/dist/latest-v14.x/docs/api/esm.html#esm_resolution_algorithm "ESModules in Node v14"
[esmodules-node-guide]: https://gils-blog.tayar.org/posts/using-jsm-esm-in-nodejs-a-practical-guide-part-1/#section-01 "ESModules in Node guide"

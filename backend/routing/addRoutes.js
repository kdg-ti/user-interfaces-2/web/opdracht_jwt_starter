import { CURRENT_USER, LOGIN_ROUTE } from "./routes";
import login from "../endpoints/login/login";
import getMe from "../endpoints/users/getMe";

export default function addRoutes(app) {
  app.post(LOGIN_ROUTE, login);
  app.get(CURRENT_USER, getMe);
}
